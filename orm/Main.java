import java.sql.SQLException;

import com.j256.ormlite.dao.*;
import com.j256.ormlite.jdbc.*;
import com.j256.ormlite.support.*;
import com.j256.ormlite.table.*;

public class Main {

	public static void main(String[] args) throws SQLException {
		String databaseUrl = "jdbc:mysql://localhost:3306";
		ConnectionSource connectionSource = new JdbcConnectionSource(databaseUrl, "root", "sammy2000");
		Account account = new Account("root", "sammy2000");
		
		try {
			Dao<Account, String> accountDao =
					DaoManager.createDao(connectionSource, Account.class);
			
			TableUtils.createTable(accountDao);
			
			connectionSource.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}