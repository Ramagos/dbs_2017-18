Vorteile:
 - man kann direkt den Input an ein Object ankoppeln und muss nicht auf SQL-Queries zur�ckgreifen
 - man muss sich nicht mit Datenbanken befassen
 - Daten liegen immer in gleicher Form vor also kann man leicht auf andere Datenbanktypen umsteigen

Nachteile:
 - Abfragen haben eine begrenzte Gr��e und Komplexit�t
 - wenn ein Fehler im ORM auftritt muss man den Code des ORMs durchsuchen, was m�hsam ist
 - eigenes CPU-Leistung verbrauchendes Programm
 - ORM nimmt viel Kontrolle also wenig Rechte und M�glichkeiten
 - nicht mit jedem System/jeder Software kompatibel
