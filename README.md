# Flatscher Samuel
*Repository f�r DBS 2017/18*

### Projekte:

| Projektname | Mitarbeiter | Beschreibung | Status |
| ------ | ------ | ------ | ------ |
| m�llunternehmen | X | Datenbank + logisches Diagramm einer M�llabfuhr | abgeschlossen |
| txtZuDatenbank | X | eine Datenbank via MySQL aus einer .txt oder .csv erstellen | abgeschlossen |
| m�llunternehmen 2 | X | mit Java-Program bestimmte Daten aus der Datenbank auslesen | in Arbeit |
| orm | X | Object Relational Mapping | in Arbeit |

### ToDos
m�llunternehmen 2: Korrektes SELECT-Statement einf�gen f�r diverse Daten
orm: zum Funktionieren bringen