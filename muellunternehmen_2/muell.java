import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.awt.*;

public class muell
{
	public static String sql;
	public static Connection c = null;
	public static Statement stmt = null;
	public static String pfad="";
	public static String dateiname=null;
	public static ArrayList<String> in = new ArrayList<String>();

	public static void main( String args[] ){
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String user = "";
			String pw = "";

			BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Bitte MySQL-User angeben: ");
			user = console.readLine();

			System.out.println("Bitte MySQL-Passwort eingeben: ");
			pw = console.readLine();

			c = DriverManager.getConnection("jdbc:mysql://localhost:3306", user, pw);

			stmt = c.createStatement();
			sql="USE mydb;";
			stmt.executeUpdate(sql);
			System.out.println("Database in use");
			stmt.close();

			System.out.println("");
			//String stat=console.readLine();

			selectMuelllasterVerfuegbar();

			c.close();
			System.err.println("End of Program");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void selectOrtLaster() throws SQLException, IOException{
		ResultSet rs;
		stmt = c.createStatement();
		sql = "";
		System.out.println(sql);
		rs = stmt.executeQuery(sql);

		while (rs.next())
			System.out.println();
		stmt.close();
	}

	public static void selectMuelllasterVerfuegbar() throws SQLException, IOException{
		ResultSet rs;
		stmt = c.createStatement();										//Select Statement ist nicht korrekt
		/*sql = "SELECT * FROM muelllaster "
				+ "WHERE zeiteinteilung.tag!=\""+getDay()+"\" "
				+ "OR zeiteinteilung.zeit<\""+getTime()+"\" "
				+ "OR zeiteinteilung.zeit>\""+getTime1()+"\";";
		*/
		sql = "SELECT * FROM muellaster;";
		rs = stmt.executeQuery(sql);

		while (rs.next())
			System.out.println(rs.getString("kennzeichen"));
		stmt.close();
	}

	public static String getDay(){
		Date d=new Date(System.currentTimeMillis());
		int n=d.getDay();
		if(n==0)
			return "SO";
		if(n==1)
			return "MO";
		if(n==2)
			return "DI";
		if(n==3)
			return "MI";
		if(n==4)
			return "DO";
		if(n==5)
			return "FR";
		if(n==6)
			return "SA";
		
		return "";
	}
	public static String getTime(){
		Date d=new Date(System.currentTimeMillis());
		int h=d.getHours();
		int m=d.getMinutes();
		return h+":"+m+":00.00";
	}
	public static String getTime1(){
		Date d=new Date(System.currentTimeMillis());
		int h=d.getHours()+1;
		int m=d.getMinutes();
		return h+":"+m+":00.00";
	}
}