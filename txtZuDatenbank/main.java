import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.awt.*;

public class main
{
	public static String sql;
	public static Connection c = null;
	public static Statement stmt = null;
	public static String pfad="";
	public static String dateiname=null;
	public static ArrayList<String> in = new ArrayList<String>();

	public static void main( String args[] ){
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String user = "";
			String pw = "";

			BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Bitte MySQL-User angeben: ");
			user = console.readLine();

			System.out.println("Bitte MySQL-Passwort eingeben: ");
			pw = console.readLine();

			c = DriverManager.getConnection("jdbc:mysql://localhost:3306", user, pw);

			stmt = c.createStatement();
			sql="CREATE DATABASE IF NOT EXISTS base1;";
			stmt.executeUpdate(sql);
			System.out.println("Database created successfully");
			stmt.close();

			stmt = c.createStatement();
			sql="USE base1;";
			stmt.executeUpdate(sql);
			System.out.println("Database in use");
			stmt.close();

			loadData();
			while(!pfad.equals("")){											//pro Tabelle
				String cols = "";
				while(in.get(0).contains("|")){										//1. Zeile (creates)
					cols += in.get(0).substring(0, in.get(0).indexOf("|")) + ",";
					in.set(0, in.get(0).substring(in.get(0).indexOf("|")+1, in.get(0).length()));
				}
				cols+=in.get(0);
				createTable(dateiname, cols);

				String values;
				for(int i=1;i<=in.size()-1;i++){									//alle weiteren Zeilen (inserts)
					values = "\"";
					while (in.get(i).contains("|")) {
						values += in.get(i).substring(0, in.get(i).indexOf("|") - 1) + "\",\"";
						in.set(i, in.get(i).substring(in.get(i).indexOf("|") + 1, in.get(i).length()));
					}
					values+=in.get(i)+"\"";
					insertTable(dateiname, values);
				}
				loadData();
			}

			saveData();

			boolean sel=true;
			while (sel) {
				System.out.println();
				System.out.println(
						"Welche Tabelle wollen sie selectieren?\n(Leerlassen, wenn sie keine Tabelle selectieren wollen)");
				String tabelle = console.readLine();
				if (!tabelle.equals("")) {
					System.out.println("Welche Spalten von \"" + tabelle
							+ "\" wollen sie selectieren?\n(Leerlassen, wenn sie alle Spalten selectieren wollen)");
					String cols = " " + console.readLine();

					System.out.println(
							"Unter welchen Bedingungen wollen sie die Ergebnisse bekommen?\n(Leerlassen, wenn sie keine Bedingungen wollen)");
					System.out.print("WHERE ");
					String bedingungen = console.readLine();

					select(tabelle, cols, bedingungen);
				} else 
					sel=false;
			}
			c.close();
			System.err.println("End of Program");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void select(String tabellenname, String spalten, String bedingungen) throws SQLException{
		System.out.println();
		stmt = c.createStatement();
		ResultSet rs;
		ArrayList<String> felder=new ArrayList<String>();
		if(spalten.equals(" ")){
			sql = "SHOW COLUMNS FROM " + tabellenname;
			rs = stmt.executeQuery(sql);

			while (rs.next())
				felder.add(rs.getString("Field"));
		} else {
			while(spalten.contains(",")){
				felder.add(spalten.substring(0, spalten.indexOf(",")));
				spalten=spalten.substring(spalten.indexOf(",")+1, spalten.length());
			}
			felder.add(spalten.substring(1,spalten.length()));
		}

		if(bedingungen.equals(""))
			sql = "SELECT * FROM " + tabellenname + ";";
		else sql = "SELECT * FROM " + tabellenname + " WHERE " + bedingungen + ";";

		rs = stmt.executeQuery(sql);

		System.out.print(" | ");
		for(int i=0;i<felder.size();i++){
			System.out.print(felder.get(i) + " | ");
		}
		System.out.println();

		while (rs.next()){
			System.out.print(" | ");
			for(int i=0;i<felder.size();i++){
				System.out.print(rs.getString(felder.get(i)) + " | ");
			}
			System.out.println();
		}
		stmt.close();
	}

	public static void createTable(String tabellenname, String spalten) throws SQLException{
		stmt = c.createStatement();
		sql="DROP TABLE IF EXISTS "+ tabellenname + ";";
		stmt.executeUpdate(sql);
		stmt.close();

		stmt = c.createStatement();
		sql = "CREATE TABLE " + tabellenname + "("
				+ spalten
				+");";
		stmt.executeUpdate(sql);
		System.out.println("Table " + tabellenname + " created successfully");
		stmt.close();
	}

	public static void insertTable(String tabellenname, String values) throws SQLException{		
		stmt = c.createStatement();
		sql = "INSERT INTO " + tabellenname + " VALUES ("
				+ values
				+");";
		stmt.executeUpdate(sql);
		System.out.println("Inserted into Table " + tabellenname + " successfully");
		stmt.close();
	}

	public static void saveData(){
		try{
			if(pfad.contains("\\.csv"))
				pfad = pfad.replaceFirst("\\.csv", "") + ".db";
			else 
				pfad = pfad.replaceFirst("\\.txt", "") + ".db";
			BufferedWriter bw=new BufferedWriter(new FileWriter(pfad));
			bw.write("\\datenbank.db");										//datenbank speichern, nicht text \datenbank.db
			bw.close();
			System.out.println("Daten erfolgreich gespeichert");
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}

	public static void loadData(){
		pfad = "";
		String einzel=null;
		boolean help=true;
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nBitte Dateipfad zur Tabelle angeben: \n(Leerlassen, wenn kein weiterer Table hinzugefügt werden soll)");

		try{
			pfad=console.readLine();
			if(!pfad.equals("")){
				BufferedReader br=new BufferedReader(new FileReader(pfad));

				dateiname = pfad;
				dateiname = dateiname.replaceFirst("\\.csv", "");
				dateiname = dateiname.replaceFirst("\\.txt", "");
				while(dateiname.contains("/")){
					dateiname=dateiname.substring(dateiname.indexOf("/")+1, dateiname.length());
				}
				while(dateiname.contains("\\")){
					dateiname=dateiname.substring(dateiname.indexOf("\\")+1, dateiname.length());
				}

				while (help) {
					einzel=br.readLine();
					if(einzel!=null)
						in.add(einzel+"");
					else help=false;
				}
				br.close();
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
			if(e.getMessage().contains("(Das System kann die angegebene Datei nicht finden)"))
				loadData();
		}
	}
}