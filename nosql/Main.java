import java.util.List;
import redis.clients.jedis.Jedis;

public class Main {

	public static Jedis jedis = new Jedis("localhost"); 
	public static void main(String[] args) {
		jedis.connect();
		System.out.println("Connection to server sucessfully");  System.out.println();
		
		createDumpTruck("MAN","x4","IL ZUS45");
		createDumpTruck("Mercedes","element","IL 123YZ");
		createDumpTruck("MAN","x5","I 48SAN");
		
//		removeDumpTruck(3);
		getAllDumpTrucks();
		System.out.println();
		getAllTrashCans();
		
		jedis.disconnect();
	}
	
	public static void createDumpTruck(String hersteller, String modell, String kennzeichen) {
		List<String> dumpTrucks=jedis.lrange("dumpTruck", 0, -1);
		int id=0;
		try{
			id = Integer.parseInt(dumpTrucks.get(dumpTrucks.size()-4))+1;
		}catch(ArrayIndexOutOfBoundsException e){
			id=1;
		}catch(Exception e) {
			System.err.println(e);
		}
		
		jedis.rpush("dumpTruck",Integer.toString(id),hersteller,modell,kennzeichen);
	}
	
	public static void getAllDumpTrucks() {
		List<String> dumpTrucks=jedis.lrange("dumpTruck", 0, -1);
		for(int i=0;i<dumpTrucks.size();i++) {
			System.out.print(dumpTrucks.get(i));
			if(i%4==3)
				System.out.println();
			else
				System.out.print(", ");
		}
	}
	
	public static void removeDumpTruck(int id) {
		List<String> dumpTrucks=jedis.lrange("dumpTruck", 0, -1);
		for(int i=0;i<dumpTrucks.size();i=i+4)
			if(dumpTrucks.get(i).equals(Integer.toString(id))) {
				for(int j=i+1;j<i+4;j++) {
					jedis.lset("dumpTruck", j, "DELETED");
					jedis.lrem("dumpTruck", 1, "DELETED");
				}
				break;
			}
	}
	
	public static void createTrashCan(String hersteller, String modell, int fassungsvermoegen, String stadtbereich) {
		List<String> trashCans=jedis.lrange("trashCan", 0, -1);
		int id=0;
		try{
			id = Integer.parseInt(trashCans.get(trashCans.size()-5))+1;
		}catch(ArrayIndexOutOfBoundsException e){
			id=1;
		}
		
		jedis.rpush("trashCan",Integer.toString(id),hersteller,modell,Integer.toString(fassungsvermoegen),stadtbereich);
	}
	
	public static void getAllTrashCans() {
		List<String> trashCans=jedis.lrange("trashCan", 0, -1);
		for(int i=0;i<trashCans.size();i++) {
			System.out.print(trashCans.get(i));
			if(i%5==4)
				System.out.println();
			else
				System.out.print(", ");
		}
	}
}